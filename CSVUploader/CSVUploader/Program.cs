﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CSVUploader
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new Form1());
		}
		private static MySqlCommand ParseCSVtoSQL(string row)
		{
			//Sätter upp parametrarna
			string[] strings = null;
			strings = row.Split(',');

			int setid = 0;

			if(strings[6] == "Awakenings") { setid = 1; }
			else if (strings[6] == "Spirit of Rebellion") { setid = 2; }
			else { throw new ArgumentException("Non-valid set"); }
			Int32.TryParse(strings[5], out int nr);
			
			//Name,Affi,Color,Type,Rarity,#,Set,Dice,Kort,Tärning,Kort,Tärningar

			//Skapar sql-command
			var cmd = new MySqlCommand();
			string sql = "INSERT INTO cards (setid, nr, name, affiliation, color, cost, text) VALUES (@pSetId, @pNr, @pName, @pAffili, @pColor, @pCost, @pText)";
			cmd.CommandText = sql;
			cmd.Parameters.AddWithValue("@pSetId", setid);
			cmd.Parameters.AddWithValue("@pNr", nr);
			cmd.Parameters.AddWithValue("@pName", strings[0]);
			cmd.Parameters.AddWithValue("@pAffili", strings[1]);
			cmd.Parameters.AddWithValue("@pColor", strings[2]);
			cmd.Parameters.AddWithValue("@pCost", null);
			cmd.Parameters.AddWithValue("@pText", null);

			return cmd;
			
		}
		public static void ReadAndUpload(string FileName)
		{
			using (System.IO.StreamReader fStream = new System.IO.StreamReader(FileName))
			{
				MySqlCommand sql = null;
				string line = null;
				var db = new DB();
				while (fStream.Peek() >= 0)
				{
					try
					{
						line = fStream.ReadLine();
						sql = ParseCSVtoSQL(line);
						db.WriteToDB(sql);
					}
					catch (Exception e)
					{
						System.Windows.Forms.MessageBox.Show(e.ToString());
					}
				}
			}
			
		}
	}
}
