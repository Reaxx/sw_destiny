﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace CLI__only_backend_.Classes
{
	class DB
	{
		#region fields
		public static string ConnectionString
		{
			get
			{
				//Connecting to the databse, fetching connectionstring from .config file
				string ConnStr = ConfigurationManager.ConnectionStrings["MySQL"].ToString();
				return ConnStr;
			}
		}
		#endregion
		#region methods
		public static MySqlConnection GetMySqlConnection()
		{
			try
			{
				MySqlConnection conn = new MySqlConnection(ConnectionString);
				conn.Open();
				return conn;
			}
			catch
			{
				//Supressing error-message that includes the user and pwd for the database
				throw new Exception("Connection to the databasae failed");
			}
		}
		#endregion

	}
}
