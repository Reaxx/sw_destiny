﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLI__only_backend_.Classes
{
	#region constructor
	/// <summary>
	/// Contains all the cards from the databaes.
	/// </summary>
	public class CardLibary : CardList
	{
		public CardLibary()
		{
			this.LoadAllCards();
		}

		private void LoadAllCards()
		{
			using (MySqlConnection conn = DB.GetMySqlConnection())
			{
				using (MySqlCommand cmd = conn.CreateCommand())
				{

					string sql = @"	SELECT cs.set, c.nr, c.name, c.affiliation, c.color, c.cost, c.text
							FROM cards as c
							JOIN cardset as cs
							ON c.setid = cs.id";
					cmd.CommandText = sql;

					MySqlDataReader rdr = cmd.ExecuteReader();

					if (!rdr.HasRows)
					{
						throw new Exception("No hits in database");
					}

					while (rdr.Read())
					{
						////Generates dictonary for easier insertion into the card-object.
						//Dictionary<string, string> CardDict = Stuff.DictForCard(rdr);
						Cards.Add(new Card(rdr));
					}
					CardsLoaded = true;
					rdr.Close();
				}
			}
		}
	}
	#endregion
}
