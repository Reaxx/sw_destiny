﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLI__only_backend_.Classes
{
	static class Stuff
	{
		/// <summary>
		/// Splits a string and returns as int array.
		/// </summary>
		/// <param name="inString">String to convert</param>
		/// <param name="splitChar">Char to split on</param>
		/// <returns>Int array</returns>
		static int[] SplitStringToInt(string inString, char splitChar)
		{
			var strings = inString.Split(splitChar);
			int[] ints = new int[strings.Length];
			for (int i = 0; i < strings.Length; i++)
			{
				Int32.TryParse(strings[i], out ints[i]);
			}

			return ints;
		}

		public static List<Deck> GetAllDecks()
		{
			using (MySqlConnection conn = DB.GetMySqlConnection())
			{
				using (MySqlCommand cmd = conn.CreateCommand())
				{
					//cmd.Connection = conn;
					string sql = @"	SELECT d.name AS deckname, p.name AS creator, d.description
									From decks AS d
									LEFT JOIN player AS p 
									ON d.creator=p.id";
					cmd.CommandText = sql;

					MySqlDataReader rdr = cmd.ExecuteReader();

					var deckList = new List<Deck>();
					if (rdr.HasRows)
					{
						while (rdr.Read())
						{
							string name = rdr["deckname"].ToString();
							string creator = rdr["creator"].ToString();
							string desc = rdr["description"].ToString();

							deckList.Add(new Deck(name,creator,desc));
						}
						
					}
					else
					{
						throw new Exception("No hits in database");
					}					
					rdr.Close();

					return deckList;
				}
			}
		}
	}
}
