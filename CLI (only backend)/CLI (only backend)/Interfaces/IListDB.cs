﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CLI__only_backend_.Classes;
namespace CLI__only_backend_.Interfaces
{
	/// <summary>
	/// Methods for CardLists to interact with the database
	/// </summary>
	interface IListDB
	{
		/// <summary>
		/// Adds a new cardlist to the database
		/// </summary>
		void InsertCardList();
		/// <summary>
		/// Loads the cardlists cards.
		/// </summary>
		void LoadCards();
		/// <summary>
		/// Updates an existing cardlist
		/// </summary>
		void UpdateCardList();
		/// <summary>
		/// Searches the object for and card, true if not found.
		/// </summary>
		/// <param name="card">Card to search for</param>
		/// <returns>True if card does not exsit in the deck</returns>
		bool IsCardNew(Card card);
		/// <summary>
		/// Loads a cardlist from the database
		/// </summary>
		void LoadCardList();
		/// <summary>
		/// Deletes the relation between the card and cardlist
		/// </summary>
		/// <param name="card">Card to remove</param>
		void RemoveCardFromDB(Card card);
		/// <summary>
		/// Uppdates an existing connection between a cardlist and card
		/// </summary>
		/// <param name="card"></param>
		void UpdateCard(Card card);
		/// <summary>
		/// Creates a new reletion between cardlist and card
		/// </summary>
		/// <param name="card"></param>
		void InsertCard(Card card);
		/// <summary>
		/// Deletes the deck and associated cardlist from the table and emtpies the cardlist in the object.
		/// </summary>
		void RemoveCardListFromDB();
	}
}
