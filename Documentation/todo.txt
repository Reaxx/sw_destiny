﻿Hög prio:
*Databas
	-Lösning för cort (med 11/13)
	-Lösning för ikoner (tärning, set)
	-Cardtext kan vara null
*Filtreringar
	-Max/Min kostnad
	-Färg
	-Affiliation
*Någon form av bytes-gränssnitt
*Bygga ut GUIet
	-Välja användare
		-Spara senaste?
	-Meny
		[Alla kort]
		[Mina kort]
			[Lista bara kort jag har]
				+- knappar för att ändra
			[Lista även dom jag inte har]
				+- knappar för att ändra
			["Rapid Fire"]
				Kundredogörelse: "Jag vill kunna knappa in nummer och trycka enter för att lägga till kortet.
				Kunna trycka enter igen för att lägga till en till. Och skriva annat nummmer utan att behöva göra annat än knappa siffror."
					Tekniskt: Markera nummret men radera inte automatisk
					Hur löser man vilket set? Stor ikon för att välja vilket set man lägger till i? Binda knapp för att switcha set?
		[Alla lekar] (senare)
		[Mina lekar] (senare)
		[Exit]
Låg prio:
*Offline mode.
	-Laddar ner datat från databasen och lagrar lokalt
*DecksToBeat